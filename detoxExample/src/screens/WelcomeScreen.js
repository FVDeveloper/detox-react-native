import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Navigation } from 'react-native-navigation';

export default class WelcomeScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View testID="screen_welcome" style={{ flex: 1, padding: 32, marginTop: 32, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontSize: 18, color: '#2d2d2d', paddingBottom: 32 }}>Seja bem vindo ao App Exemplo Detox</Text>
                <Text style={{ fontSize: 14, color: '#2d2d2d', paddingBottom: 32 }}>Clique abaixo para realizar o Login</Text>
                <TouchableOpacity
                    style={{ padding: 16, marginHorizontal: 32, marginVertical: 32, width: '100%', backgroundColor: '#ff8201', alignItems: 'center' }}
                    testID="welcome_btn_login"
                    onPress={() => { Navigation.push(this.props.componentId, { component: { name: 'screens.LoginScreen' } }) }}>
                    <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>Ir para o Login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}