import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput, Keyboard } from 'react-native';
import { Navigation } from 'react-native-navigation';

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login: '',
            loginIsValid: true,
            password: '',
            passwordIsValid: true
        }
    }

    onLoginPress() {
        Keyboard.dismiss()
        const { login, password } = this.state;

        this.setState({ loginIsValid: true, passwordIsValid: true }, () => {
            this.setState({ loginIsValid: !!login, passwordIsValid: !!password }, () => {
                const { loginIsValid, passwordIsValid } = this.state;
                (loginIsValid && passwordIsValid) && Navigation.push(this.props.componentId, { component: { name: 'screens.SuccessScreen', passProps: { login } } })
            });
        })
    }

    render() {
        const { login, loginIsValid, password, passwordIsValid } = this.state;
        return (
            <View testID="screen_login" style={{ flex: 1, padding: 32, marginTop: 64, alignItems: 'center' }}>
                <Text style={{ fontSize: 18, color: '#2d2d2d', paddingBottom: 32 }}>Realize o seu login</Text>
                <Text style={{ fontSize: 14, color: '#2d2d2d', paddingBottom: 8, alignSelf: 'flex-start' }}>Login</Text>
                <TextInput testID="login_txt_login" style={{ fontSize: 18, color: '#2d2d2d', width: '100%', marginHorizontal: 32, paddingBottom: 32 }}
                    placeholder='Digite o seu login'
                    placeholderTextColor='#b2b2b2'
                    value={login}
                    onChangeText={login => this.setState({ login })}
                />
                {!loginIsValid && <Text style={{ fontSize: 12, color: '#cc3030', paddingBottom: 16, alignSelf: 'flex-start' }}>Login obrigatório!</Text>}
                <Text style={{ fontSize: 14, color: '#2d2d2d', paddingBottom: 8, alignSelf: 'flex-start' }}>Senha</Text>
                <TextInput testID="password_txt_email" style={{ fontSize: 18, color: '#2d2d2d', width: '100%', marginHorizontal: 32, paddingBottom: 32 }}
                    secureTextEntry
                    placeholder='Digite sua senha'
                    placeholderTextColor='#b2b2b2'
                    value={password}
                    onChangeText={password => this.setState({ password })}
                />
                {!passwordIsValid && <Text style={{ fontSize: 12, color: '#cc3030', paddingBottom: 16, alignSelf: 'flex-start' }}>Senha obrigatória!</Text>}
                <TouchableOpacity
                    style={{ padding: 16, marginHorizontal: 32, marginVertical: 32, width: '100%', backgroundColor: '#ff8201', alignItems: 'center' }}
                    testID="login_signin_button"
                    onPress={() => { this.onLoginPress() }}>
                    <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>Entrar</Text>
                </TouchableOpacity>
            </View>
        )
    }
}