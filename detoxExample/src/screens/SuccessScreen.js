import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class SuccessScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View testID="screen_success" style={{ flex: 1, padding: 32, marginTop: 32, alignItems: 'center', justifyContent: 'center' }}>
                <Text testID="success_lbl_title" style={{ fontSize: 18, color: '#2d2d2d', paddingBottom: 32 }}>Parabéns, {this.props.login}</Text>
                <Text style={{ fontSize: 14, color: '#2d2d2d', paddingBottom: 32, textAlign: 'center' }}>Você concluiu o mini-tutorial Detox para React-Native</Text>
            </View>
        )
    }
}