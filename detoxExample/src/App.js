import { Navigation } from "react-native-navigation";
import WelcomeScreen from "./screens/WelcomeScreen";
import LoginScreen from "./screens/LoginScreen";
import SuccessScreen from './screens/SuccessScreen';

Navigation.registerComponent(`screens.WelcomeScreen`, () => WelcomeScreen);
Navigation.registerComponent(`screens.LoginScreen`, () => LoginScreen);
Navigation.registerComponent(`screens.SuccessScreen`, () => SuccessScreen);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setDefaultOptions({
    topBar: {
      visible: false,
      drawBehind: true,
      animate: false
    }
  });
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'screens.SuccessScreen'
            },
            component: {
              name: "screens.LoginScreen"
            },
            component: {
              name: "screens.WelcomeScreen"
            }
          }
        ]
      }
    }
  });
});