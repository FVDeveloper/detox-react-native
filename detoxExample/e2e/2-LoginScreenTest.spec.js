describe('LoginScreen', () => {

    it('should have login screen', async () => {
        await expect(element(by.id('screen_login'))).toBeVisible();
    });

    it('should have login screen title', async () => {
        await expect(element(by.text('Realize o seu login'))).toBeVisible();
    });

    it('should have field login label', async () => {
        await expect(element(by.text('Login'))).toBeVisible();
    });

    it('should have field login', async () => {
        await expect(element(by.id('login_txt_login'))).toBeVisible();
    });

    it('should have field password label', async () => {
        await expect(element(by.text('Senha'))).toBeVisible();
    });

    it('should have field password', async () => {
        await expect(element(by.id('password_txt_email'))).toBeVisible();
    });

    it('should show login error label and password error label after tap', async () => {
        await element(by.id('login_txt_login')).typeText('');
        await element(by.id('password_txt_email')).typeText('');
        await element(by.id('login_signin_button')).tap();
        await waitFor(element(by.text('Login obrigatório!'))).toBeVisible().withTimeout(10000);
        await waitFor(element(by.text('Senha obrigatória!'))).toBeVisible().withTimeout(10000);
    });

    it('should show success screen after tap', async () => {
        await element(by.id('login_txt_login')).typeText('Viadão');
        await element(by.id('password_txt_email')).typeText('senha123');
        await element(by.id('login_signin_button')).tap();
        await waitFor(element(by.id('screen_success'))).toBeVisible().withTimeout(10000);
    });
});