describe('WelcomeScreen', () => {
    it('should have welcome screen', async () => {
        await expect(element(by.id('screen_welcome'))).toBeVisible();
    });

    it('should have welcome screen title', async () => {
        await expect(element(by.text('Seja bem vindo ao App Exemplo Detox'))).toBeVisible();
    });

    it('should have welcome screen subtitle', async () => {
        await expect(element(by.text('Clique abaixo para realizar o Login'))).toBeVisible();
    });

    it('should show login screen after tap', async () => {
        await element(by.id('welcome_btn_login')).tap();
        await waitFor(element(by.text('Realize o seu login'))).toBeVisible().withTimeout(10000);
    });
});