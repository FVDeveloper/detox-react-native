describe('SuccessScreen', () => {
   
    it('should have success screen', async () => {
        await expect(element(by.id('screen_success'))).toBeVisible();
    });

    it('should have success screen title', async () => {
        await expect(element(by.id('success_lbl_title'))).toBeVisible();
    });

    it('should have success screen subtitle', async () => {
        await expect(element(by.text('Você concluiu o mini-tutorial Detox para React-Native'))).toBeVisible();
    });
});